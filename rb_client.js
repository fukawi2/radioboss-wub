const PLAYBACK_STATUS_INTERVAL_SECS = 3;
const PLAYLIST_INTERVAL_SECS = 14;
const FADEOUT_TIME_SECS = 3;

var we_are_configured = false;
var last_action_desc;
var apiHost;
var apiPort;
var apiPass;
var automation_group;

// to prevent multiple timers firing the recurring functions, we must
// track the id of each timer, and only set a new timer if the id is
// not already set. see initialize() function for where these are used
var ps_interval_id;
var pl_interval_id;


// javascript doesn't have a sprintf implementation, so emulate one.
// source: https://stackoverflow.com/questions/31040306/sprintf-equivalent-for-client-side-javascript
var sprintf = (str, ...argv) => !argv.length ? str :
sprintf(str = str.replace(sprintf.token||"$", argv.shift()), ...argv);

function basename(str) {
  if (str == null)
    return;
  var base = new String(str).substring(str.lastIndexOf('\\') + 1);
  if(base.lastIndexOf(".") != -1)
    base = base.substring(0, base.lastIndexOf("."));
  return base;
}

// formats a message in a useful format to console.log
var DebugLog = function(msg) {
  var today = new Date();
  var time  = ('0' + today.getHours()).slice(-2) + ":"
            + ('0' + today.getMinutes()).slice(-2) + ":"
            + ('0' + today.getSeconds()).slice(-2);
  console.log(sprintf('$ - $', time, msg));
}

$(document).ready(function(){
  initialize();
});

/**************************************************************************
 * initializes the whole web ui; called by document.ready() as well
 * as after changing options
 *************************************************************************/
var initialize = function() {
  $('#rb_status').empty();
  $('#playlist').toggleClass('loading', true);

  // get configuration form localStorage
  LoadOptions();

  getVersion();
  UpdatePlaybackStatus();
  UpdatePlaylist(); // also calls UpdateEventLog()

  if (!ps_interval_id) {
    DebugLog('Setting timer for UpdatePlaybackStatus() to ' + PLAYBACK_STATUS_INTERVAL_SECS + ' seconds');
    ps_interval_id = setInterval(UpdatePlaybackStatus, PLAYBACK_STATUS_INTERVAL_SECS*1000);
  };
  if (!pl_interval_id) {
    DebugLog('Setting timer for UpdatePlaylist() to ' + PLAYLIST_INTERVAL_SECS + ' seconds');
    pl_interval_id = setInterval(UpdatePlaylist, PLAYLIST_INTERVAL_SECS*1000);
  };
};

/**************************************************************************
 * functions for handling the options modal dialog
 *************************************************************************/
var ShowOptions = function() { $('#options-modal').addClass('active'); };
var HideOptions = function() { $('#options-modal').removeClass('active'); };
var LoadOptions = function() {
  // load configuration options from localStorage
  station_name  = localStorage.getItem('station_name');
  apiHost       = localStorage.getItem('rb_host');
  apiPort       = localStorage.getItem('rb_port');
  apiPass       = localStorage.getItem('rb_password');
  automation_group = localStorage.getItem('automation_group');

  // we can guess some defaults
  if (!apiHost) apiHost = 'localhost';
  if (!apiPort) apiPort = '9000';

  // put the configuration values into the form elements of the options window
  $('#opt_station_name').val(station_name);
  $('#opt_api_host').val(apiHost);
  $('#opt_api_port').val(apiPort);
  $('#opt_api_pass').val(apiPass);
  $('#opt_automation_group').val(automation_group);

  // make sure we have all our required options, otherwise show options window
  if (station_name && apiHost && apiPort && apiPass) {
    we_are_configured = true; // false by default; prevents other functions from making calls to the api if we don't have required settings
  } else {
    ShowOptions();
  };

  // disable things we can't do due to missing config
  if (!automation_group) {
    $('#automation_enable').prop('disabled', true);
    $('#automation_disable').prop('disabled', true);
  } else {
    $('#automation_enable').prop('disabled', false);
    $('#automation_disable').prop('disabled', false);
  };

  // use the configured options to setup the web ui
  $('#station_name').html(station_name);
};
var SaveOptions = function() {
  localStorage.setItem('station_name', $('#opt_station_name').val());
  localStorage.setItem('rb_host', $('#opt_api_host').val());
  localStorage.setItem('rb_port', $('#opt_api_port').val());
  localStorage.setItem('rb_password', $('#opt_api_pass').val());
  localStorage.setItem('automation_group', $('#opt_automation_group').val());
  HideOptions();
  initialize();
};

/**************************************************************************
 * make a HTTP GET to the radioboss api
 *************************************************************************/
var CallAPI = function(request, callback) {
  if (we_are_configured === false) return false; // dont make queries if we're not configured

  if (callback === undefined) callback = result_handler;

  api_uri = sprintf('http://$:$/?pass=$&$', apiHost, apiPort, apiPass, request);
  DebugLog('Making API request: '+ api_uri);
  $.get(api_uri, callback);
}

/**************************************************************************
 * callback to handle the result of calls the to radioboss api
 *************************************************************************/
var result_handler = function(data){
  DebugLog('result_handler() data: '+ data);
  if (data == 'OK') {
    $('#rb_status').toggleClass('text-success', true);
    $('#rb_status').toggleClass('text-error', false);
  } else {
    $('#rb_status').toggleClass('text-success', false);
    $('#rb_status').toggleClass('text-error', true);
  }

  // other functions can set `last_action_desc` so we can provide feedback
  // to the user on what `data` is in reference to
  if (last_action_desc) {
    output = sprintf('$: $', last_action_desc, data);
    last_action_desc = null;
  } else {
    output = data;
  }

  $('#rb_status').html(output);
  $('#rb_status').show();
  $('#rb_status').delay(3000).fadeOut();
};

/**************************************************************************
 * given artist, title, filename and "cast title", return a formatted
 * string to display on the page that is sane and 'friendly'
 *************************************************************************/
var MakeFriendlyTitle = function(artist, title, fname, cast_title) {
  if (typeof artist === 'undefined') artist = '';
  if (typeof title === 'undefined') title = '';
  if (typeof cast_title === 'undefined') cast_title = '';
  artist = artist.trim();
  title = title.trim();
  cast_title = cast_title.trim();
  if (fname == 'stop.command') return '(stop)';
  if (artist == '' && title == '' && cast_title != '') return cast_title;
  if (artist == '' && title == '') return basename(fname);
  return sprintf('$ - $', artist, title);
}

/**************************************************************************
 * fetches the current playback status and updates the ui
 *************************************************************************/
var UpdatePlaybackStatus = function() {
  CallAPI('action=playbackinfo', function(data) {
    xml = $(data);
    // previous track
    artist = xml.find('Info').find('PrevTrack').find('TRACK').attr('ARTIST');
    title  = xml.find('Info').find('PrevTrack').find('TRACK').attr('TITLE');
    cast_title  = xml.find('Info').find('PrevTrack').find('TRACK').attr('CASTTITLE');
    old_ptrack = $('#ptrack').html();
    new_ptrack = MakeFriendlyTitle(artist, title, null, cast_title);
    // current track
    artist = xml.find('Info').find('CurrentTrack').find('TRACK').attr('ARTIST');
    title  = xml.find('Info').find('CurrentTrack').find('TRACK').attr('TITLE');
    cast_title  = xml.find('Info').find('CurrentTrack').find('TRACK').attr('CASTTITLE');
    old_ctrack = $('#ctrack').html();
    new_ctrack = MakeFriendlyTitle(artist, title, null, cast_title);
    // next track
    artist = xml.find('Info').find('NextTrack').find('TRACK').attr('ARTIST');
    title  = xml.find('Info').find('NextTrack').find('TRACK').attr('TITLE');
    cast_title  = xml.find('Info').find('NextTrack').find('TRACK').attr('CASTTITLE');
    old_ntrack = $('#ntrack').html();
    new_ntrack = MakeFriendlyTitle(artist, title, null, cast_title);

    // only change html values if there are actually changes. if there are
    // changes then update the playlist as well.
    update_playlist = false;
    if (old_ptrack != new_ptrack) {
      $('#ptrack').html(new_ptrack);
      update_playlist = true;
    }
    if (old_ctrack != new_ctrack) {
      $('#ctrack').html(new_ctrack);
      update_playlist = true;
    }
    if (old_ntrack != new_ntrack) {
      $('#ntrack').html(new_ntrack);
      update_playlist = true;
    }

    play_status  = xml.find('Info').find('Playback').attr('state');
    switch(play_status) {
      case 'stop':
        $('#rb_stop').toggleClass('bg-error', true);
        $('#rb_play').toggleClass('bg-success', false);
        $('#ctrack').empty(); // if player is stopped, there is no current track
        update_playlist = false; // dont bother if player is stopped; wait for timer
        break;
      case 'play':
        $('#rb_stop').toggleClass('bg-error', false);
        $('#rb_play').toggleClass('bg-success', true);
        break;
    }

    if (update_playlist)
      UpdatePlaylist();
  });
}

/**************************************************************************
 * fetches the current playlist and updates the ui
 *************************************************************************/
var UpdatePlaylist = function() {
  CallAPI('action=getplaylist&to=11', function(data) {
    var new_content = '<h3>Next 10 Tracks</h3><ol>';
    xml = $(data);
    xml.find('Playlist').find('TRACK').each(function(index, row) {
      if (index == 0) return; // skip the currently playing item
      artist = $(row).attr('ARTIST');
      title = $(row).attr('TITLE');
      fname = $(row).attr('FILENAME');
      cast_title = $(row).attr('CAST_TITLE');

      display_text = MakeFriendlyTitle(artist, title, fname, cast_title);
      new_content = new_content.concat(sprintf('<li>$</li>', display_text));
    });
    new_content = new_content.concat('</ol>');
    if ($('#playlist').html() != new_content) {
      DebugLog('Playlist changed');
      $('#playlist').html(new_content);
    } else {
      DebugLog('No change to playlist');
    }
    $('#playlist').toggleClass('loading', false);
  });
  UpdateEventLog();
}

/**************************************************************************
 * fetches the current event log and updates the ui
 *************************************************************************/
var UpdateEventLog = function() {
  CallAPI('action=getlog', function(data) {
    var new_content = '';
    var xml = $(data);
    xml.find('Log').find('Item').each(function(index, row) {
      event_type = $(row).attr('type');
      event_ts = $(row).attr('time');
      event_msg = $(row).attr('text');
      if (event_type)
        new_content = (sprintf("$ - $ ($)<br/>", event_ts, event_msg, event_type)) + new_content;
      else
        new_content = (sprintf("$ - $<br/>", event_ts, event_msg)) + new_content;
    });
    $('#rb_eventlog').html(new_content);
    DebugLog('Updated Event Log');
  });
}

/**************************************************************************
 * queries radioboss for it's current version and displays in #rb_version
 *************************************************************************/
var getVersion = function(){
  CallAPI('action=status', function(data) {
    xml = $(data);
    $('#rb_version').html('RadioBoss Version ' + xml.find('Status').find('Player').attr('version'));
  });
};

/**************************************************************************
 * individual button events
 *************************************************************************/
$('#rb_play').click(function(e) { CallAPI('cmd=play'); last_action_desc='Play'; UpdatePlaybackStatus(); });
$('#rb_stop').click(function(e) { CallAPI('cmd=stop'); last_action_desc='Stop'; UpdatePlaybackStatus(); });
$('#rb_prev').click(function(e) { CallAPI('cmd=prev'); last_action_desc='Previous'; UpdatePlaybackStatus(); });
$('#rb_next').click(function(e) { CallAPI('cmd=next'); last_action_desc='Next'; UpdatePlaybackStatus(); });

$('#fade_out').click(function(e) {
  CallAPI('cmd=fadeout ' + FADEOUT_TIME_SECS*1000);
  last_action_desc='Fadeout';
  UpdatePlaybackStatus();
  UpdatePlaylist();
});
$('#skip_next').click(function(e) {
  CallAPI('action=delete&pos=2');
  last_action_desc='Skip Next';
  UpdatePlaylist();
  UpdatePlaybackStatus();
});
$('#automation_enable').click(function(e) { CallAPI('cmd=enablegroup ' + encodeURIComponent(automation_group)); });
$('#automation_disable').click(function(e) { CallAPI('cmd=disablegroup ' + encodeURIComponent(automation_group)); });

$("#show_options").on("click", function(e) {
  ShowOptions();
});
$("#save_options").on("click", function(e) {
  e.preventDefault();
  SaveOptions();
});

// .close-modal class can be used for any modal dialog to avoid code duplication
$(".close-modal").on("click", function(e) { $(this).closest('.modal').removeClass('active'); });
