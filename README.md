# RadioBoss-WUB

RadioBoss WebUI Basic (WUB). A basic html and javascript control interface for RadioBoss.

## Requirements

- RadioBoss-WUB is written purely in HTML, CSS and JavaScript. This means it should
run directly in any modern browser.
- The computer running RadioBoss-WUB must have network access to the RadioBoss API.
This could be directly on the same local network, but can also be achieved remotely
by either port-forwarding on your router, or a VPN connection. These methods are
beyond the scope of this document.
- RadioBoss 5.8.4.0 or greater

## Download

Download the current revision as a ZIP file directly from GitLab:

https://gitlab.com/fukawi2/radioboss-wub/-/archive/master/radioboss-wub-master.zip

## Installation

1. Extract all files to a directory of your preference.
2. Open `index.html` in your favorite browser.
3. The options window will open; enter details for your site and click save.

You can also host the files on a web server (eg, *nginx* or *apache*) if needed,
but the *client* must still have network access to the RadioBoss API.

## Components

- SpectreCSS: https://picturepan2.github.io/spectre/
- jQuery: https://jquery.com/

## Author

Phillip Smith <fukawi2@gmail.com>

## License

See LICENSE file.
